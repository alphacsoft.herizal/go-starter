package entity

import "gorm.io/gorm"

type User struct {
	gorm.Model
	Email    string `gorm:"uniqueIndex" json:"email"`
	FullName string `json:"full_name"`
	Username string `json:"username"`
	Password string `json:"password"`
}
