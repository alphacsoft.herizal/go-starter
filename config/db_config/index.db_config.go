package db_config

import "os"

var DB_DRIVER = "psql"
var DB_HOST = "localhost"
var DB_NAME = "Your database name"
var DB_USER = "Your database user"
var DB_PASSWORD = "Your database password"
var DB_PORT = "5432"

func InitDatabaseConfig() {

	hostEnv := os.Getenv("DB_HOST")
	if hostEnv != "" {
		DB_HOST = hostEnv
	}

	nameEnv := os.Getenv("DB_NAME")
	if nameEnv != "" {
		DB_NAME = nameEnv
	}

	userEnv := os.Getenv("DB_USER")
	if userEnv != "" {
		DB_USER = userEnv
	}

	passwordEnv := os.Getenv("DB_PASSWORD")
	if passwordEnv != "" {
		DB_PASSWORD = passwordEnv
	}

	portEnv := os.Getenv("DB_PORT")
	if portEnv != "" {
		DB_PORT = portEnv
	}
}
