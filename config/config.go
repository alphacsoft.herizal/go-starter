package config

import (
	"gitlab.com/alphacsoft.herizal/go-starter/config/app_config"
	"gitlab.com/alphacsoft.herizal/go-starter/config/db_config"
)

func InitConfig() {

	app_config.InitAppConfig()
	db_config.InitDatabaseConfig()

}
