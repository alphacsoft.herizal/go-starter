package routes

import (
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/alphacsoft.herizal/go-starter/controller/user_controller"
)

func InitRoutes(app *gin.Engine) {

	routes := app.Group(os.Getenv("BaseUrl"))

	// AUTH
	// USER
	userRoutes := routes.Group("/user")
	userRoutes.GET("/", user_controller.FindUser)

}
