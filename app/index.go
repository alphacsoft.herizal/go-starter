package app

import (
	"log"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/alphacsoft.herizal/go-starter/config"
	"gitlab.com/alphacsoft.herizal/go-starter/database"
	"gitlab.com/alphacsoft.herizal/go-starter/routes"
)

func Run() {

	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading environment variables")
	} else {
		log.Println("Success loading environment variables")
	}

	// INIT CONFIG
	config.InitConfig()

	// Init Database
	database.ConnectDatabase()

	app := gin.Default()

	routes.InitRoutes(app)

	app.Run(":8080")
}
