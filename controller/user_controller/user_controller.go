package user_controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/alphacsoft.herizal/go-starter/database"
	"gitlab.com/alphacsoft.herizal/go-starter/helper"
	"gitlab.com/alphacsoft.herizal/go-starter/models/entity"
)

func FindUser(ctx *gin.Context) {

	user := new([]entity.User)

	if err := database.DB.Find(&user).Error; err != nil {
		helper.Response(ctx.Writer, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	helper.Response(ctx.Writer, http.StatusOK, "Successfully fetch all user data", user)
}
